// copyFilePath.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "windows.h"

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc!=2)
	{
		return 1;
	}
	if (OpenClipboard(NULL))//打开剪切板
	{
		EmptyClipboard();//清空剪切板,让当前窗口进程拥有剪切板
		HANDLE hClip;
		hClip = GlobalAlloc(GMEM_MOVEABLE, (_tcslen(argv[1]) + 1)*sizeof(TCHAR));//分配内存对象返回地址(该函数是系统从全局堆中分配内存)
		TCHAR *pBuf;
		pBuf = (TCHAR *)GlobalLock(hClip);//锁定全局内存中指定的内存块，并返回一个地址值，令其指向内存块的起始处
		_tcscpy_s(pBuf, _tcslen(argv[1]) + 1, argv[1]);//将Str对象中的数据Copy到内存空间中
		GlobalUnlock(hClip);//解锁全局内存块
		SetClipboardData(CF_UNICODETEXT, hClip);//设置剪贴板数据
		CloseClipboard();//关闭
	}
	return 0;
}

